/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kreginal <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/26 15:10:16 by kreginal          #+#    #+#             */
/*   Updated: 2021/10/27 16:13:05 by kreginal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "libft.h"

// Функция ft_memcpy копирует n байт из массива (области памяти), на который 
// Указывает аргумент src, в массив (область памяти), на который указывает 
// Аргумент dst. Если массивы перекрываются, 
// Результат копирования будет не определен.
void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	unsigned char	*str1;
	unsigned char	*str2;
	size_t			pos;

	pos = 0;
	str1 = (unsigned char *)src;
	str2 = (unsigned char *)dst;
	if (dst == src)
		return (dst);
	while (pos < n)
	{
		str2[pos] = str1[pos];
		pos++;
	}
	return (dst);
}

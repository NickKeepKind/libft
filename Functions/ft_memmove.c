/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kreginal <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/12 16:01:41 by kreginal          #+#    #+#             */
/*   Updated: 2021/10/26 16:44:09 by kreginal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "libft.h"

// ft_memcpy.c
static void	*ft_memjaba(void *dst, const void *src, size_t n)
{
	unsigned char	*dst1;
	unsigned char	*src1;
	size_t			pos;

	pos = 0;
	src1 = (unsigned char *)src;
	dst1 = (unsigned char *)dst;
	if (dst == src)
		return (dst);
	while (pos < n)
	{
		dst1[pos] = src1[pos];
		pos++;
	}
	return (dst);
}

// Функция ft_memmove копирует n байт из массива (области памяти), 
// На который указывает аргумент src, в массив (область памяти),
// На который указывает аргумент dst. 
// При этом массивы (области памяти) могут пересекаться.
void	*ft_memmove(void *dst, const void *src, size_t len)
{
	unsigned char	*str1;
	unsigned char	*str2;

	str1 = (unsigned char *) dst;
	str2 = (unsigned char *) src;
	if (str2 < str1)
		while (len--)
			str1[len] = str2[len];
	else
		ft_memjaba(str1, str2, len);
	return (dst);
}

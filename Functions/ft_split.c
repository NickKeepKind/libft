/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kreginal <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/22 16:11:13 by kreginal          #+#    #+#             */
/*   Updated: 2021/11/08 19:06:50 by kreginal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "libft.h"

// Функция считает колиечство слов (j) пропуская разделители.
static int	ft_words(const char *str, char c)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (str[i])
	{
		if (str[i] == c)
			i++;
		else
		{
			j++;
			while (str[i] != '\0' && str[i] != c)
				i++;
		}
	}
	return (j);
}

// Функция выделяет кажду букву слова сохраняя его в памяти.
static char	*my_words(const char *str, char c)
{
	int		i;
	char	*letter;

	i = 0;
	while (*str && *str == c)
		str++;
	while (str[i] && str[i] != c)
		i++;
	letter = malloc(sizeof(char) * (i + 1));
	if (letter == NULL)
		return (NULL);
	i = 0;
	while (str[i] && str[i] != c)
	{
		letter[i] = str[i];
		i++;
	}
	letter[i] = '\0';
	return (letter);
}

// Функция освобождение памяти.
static void	free_words(int i, char **ptr)
{
	while (i > 0)
	{
		free(ptr[i - 1]);
		i--;
	}
	free(ptr);
}

// Функция ft_split выделяет (с помощью malloc(3)) и возвращает массив
// Cтрок, полученных путем разделения "s" с использованием cимвола 
// "c" в качестве разделителя. Массив должен заканчиваться 
// НУЛЕВЫМ указателем.
char	**ft_split(char const *s, char c)
{
	char	**find_words;
	int		words;
	int		i;

	if (!s)
		return (NULL);
	words = ft_words(s, c);
	find_words = malloc(sizeof(char *) * (words + 1));
	if (find_words == NULL)
		return (NULL);
	i = 0;
	while (i < words)
	{
		while (*s && *s == c)
			s++;
		find_words[i] = my_words(s, c);
		if (find_words[i] == NULL)
			free_words(i, find_words);
		while (*s && *s != c)
			s++;
		i++;
	}
	find_words[i] = NULL;
	return (find_words);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kreginal <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 15:03:18 by kreginal          #+#    #+#             */
/*   Updated: 2021/11/09 16:40:34 by kreginal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "libft.h"

// ft_strlen.c
static size_t	ft_lenkos(const char *s)
{
	size_t	i;

	i = 0;
	while (*s++)
		i++;
	return (i);
}

// Выделяет (с помощью malloc(3)) и возвращает подстроку из строки "s".
// Подстрока начинается с индекса "s" и имеет максимальный размер "len’.
char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	size_t	i;
	size_t	size_len;
	size_t	size;
	char	*tab;

	if (!s)
		return (NULL);
	i = 0;
	size_len = ft_lenkos(s);
	size = ft_lenkos(s + start);
	if (len > size)
		len = size;
	if (start >= size_len)
		len = 0;
	tab = malloc(sizeof(char) * len + 1);
	if (!tab)
		return (NULL);
	while (i < len)
	{
		tab[i] = s[start + i];
		i++;
	}
	tab[i] = '\0';
	return (tab);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kreginal <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/14 15:30:05 by kreginal          #+#    #+#             */
/*   Updated: 2021/11/09 18:07:46 by kreginal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "libft.h" 

// ft_memset.c
static void	*ft_setjabas(void *b, int c, size_t len)
{
	unsigned char	*str;
	unsigned char	chr;
	size_t			i;

	str = b;
	chr = c;
	i = 0;
	while (i < len)
	{
		str[i] = chr;
		i++;
	}
	return (b);
}

// ft_bzero.c
static void	ft_zero(void *s, size_t n)
{
	ft_setjabas(s, 0, n);
}

// Функция ft_calloc распределяет память для массива размером nmemb,
// Каждый элемент которого равен size байтов, и возвращает указатель
// На распределенную память. Память при этом "очищается".
void	*ft_calloc(size_t count, size_t size)
{
	void	*m;

	m = malloc(count * size);
	if (!m)
		return (NULL);
	ft_zero(m, (count * size));
	return (m);
}

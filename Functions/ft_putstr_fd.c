/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kreginal <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 14:27:28 by kreginal          #+#    #+#             */
/*   Updated: 2021/10/24 14:40:30 by kreginal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "libft.h"

// ft_putchar_fd.c
static void	ft_putjaba_fd(char c, int fd)
{
	write(fd, &c, 1);
}

// Функция ft_putstr_fd выводит строку s в файловый дискриптор fd.
void	ft_putstr_fd(char *s, int fd)
{
	int	j;

	if (fd < 0 || !s)
		return ;
	j = 0;
	while (s[j] != '\0')
	{
		ft_putjaba_fd(s[j], fd);
		j++;
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kreginal <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/24 16:57:26 by kreginal          #+#    #+#             */
/*   Updated: 2021/10/26 20:44:06 by kreginal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "libft.h"

// Функция ищет все пробелы по таблице ASCII.
static int	ft_isspace(int c)
{
	return (c == ' ' || ('\t' <= c && c <= '\r'));
}

// Функция atoi конвертирует строку, на которую указывает параметр str, 
// В величину типа int. Строка должна содержать корректную запись целого числа. 
// В противном случае возвращается 0. Число может завершаться любым символом,
// Который не может входить в состав строкового представления целого числа.
// Сюда относятся пробелы, знаки пунктуации и другие знаки, 
// Не являющиеся цифрами.
int	ft_atoi(const char *str)
{
	unsigned int	i;
	int				pos;
	int				res;

	i = 0;
	res = 0;
	pos = 1;
	while (ft_isspace(str[i]))
		i++;
	if (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			pos = -1;
		i++;
	}
	while (str[i] >= '0' && str[i] <= '9')
	{
		res = res * 10 + str[i] - '0';
		i++;
	}
	return ((int)(res * pos));
}
